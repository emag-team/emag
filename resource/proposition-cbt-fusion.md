# Proposition de fusion du système de combat

1. Un enemi rentre dans le champ de vision ( tous les enemis partagemt-ils tous le même champ de vision ?), placement des piliers à chaque extremité, zone de combat, zone de danger differentes par ennemis, agresivité.
2. Test d'initiative à chaque début de tour (aspect stratégique important)
   * On peut upper l'init avec des buffs, pour pouvoir enchainer 2 tours.
3. Une **action/compétence** necessite eventuellement :
   * Des PA.
   * Un certain niveau d'une ou plusieurs carrac
   * Un jet de dé sur une ou plusieurs carrac
4. On peut payer une **action/compétence** qui va se déclencher pendant le tour adverse :
   * Une contre-attaque
   * Une défense
   * ...
5. À la fin du tour on récupère des PA, plus ou moins selon le personnage, on peut eventuellement booster la récup de PA.
6. On peut faire plusieurs actions dans notre tour de jeu, si on dépense beaucoup de PA

Une action n'est pas obligatoirement réussie.

## À réflechir

* Comment doper/buffer/upper l'init, les differenter carrac ou les action/compétence ?
* CC et fumble